import { Component, Input } from '@angular/core';

@Component({
  selector: 'tt-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {
  @Input() elevation: number;
}
